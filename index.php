<!DOCTYPE html>
<html>

    <head lang="fr">
        <meta charset="UTF-8">

        <title>Default Template Project</title>

        <!-- CSS Imports -->
        <link rel="stylesheet" href="theme/css/global.css"/>
        <!-- ***PUT YOUR CSS HERE*** -->
        <!-- CSS Imports End -->
    </head>

    <body>        

    </body>

    <!-- Scripts Imports -->
    <script src="scripts/vendors/jquery.min.js"></script>
    <script src="scripts/vendors/materialize.min.js"></script>
    <!-- ***PUT YOUR SCRIPTS HERE*** -->
    <!-- Scripts Imports End -->
</html>