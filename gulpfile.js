/***************************
 *  Module Requirements
 **************************/

var gulp =      require('gulp');
var live =      require('gulp-livereload');
var prefixer =  require('gulp-autoprefixer');
var sass =      require('gulp-sass');

/***************************
 *  Config
 **************************/

var __scss_watch =      'theme/scss/**/*.scss';
var __css_dir =         'theme/css';
var __reload_watch =    [
                            'index.html',
                            'index.php',
                            'templates/**/*.html',
                            'templates/**/*.php',
                            'theme/css/**/*.css',
                            'theme/scripts/**/*.js'
                        ];

/***************************
 *  Gulp Tasks
 **************************/

/**** Default ****/
 gulp.task('default', ['sass'], function(){});

/**** Watch ****/
gulp.task('watch', ['default'], function() {
    live.listen();

    // files watch for livereload
    gulp.watch(__reload_watch)
        .on('change', function(event){
            live.changed(event.path);
        })

    // scss files watch for compile to css
    gulp.watch(__scss_watch, ['sass'])
        .on('change', function(event){
            console.info('-----Change detected on "' + event.path +'"-----');
        });
});

/**** SASS ****/
gulp.task('sass', function(){
    return gulp.src(__scss_watch)
        .pipe(sass({
            onError: console.error.bind(console, 'SASS Error:')
        }))
        .pipe(prefixer({
            browsers: ['last 2 versions']
        }))
        .pipe(gulp.dest(__css_dir));
});



